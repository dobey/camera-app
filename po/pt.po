# Portuguese translation for camera-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-23 17:57+0000\n"
"PO-Revision-Date: 2020-05-11 21:23+0000\n"
"Last-Translator: Ivo Xavier <ivofernandes12@gmail.com>\n"
"Language-Team: Portuguese <https://translate.ubports.com/projects/ubports/"
"camera-app/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: ../AdvancedOptions.qml:16
msgid "Settings"
msgstr "Definições"

#: ../AdvancedOptions.qml:48
msgid "Add date stamp on captured images"
msgstr "Adicionar marca de data nas imagens capturadas"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:70
msgid "Format"
msgstr "Formato"

#: ../AdvancedOptions.qml:122
msgid "Date formatting keywords"
msgstr "Palavras-chave de formação da data"

#: ../AdvancedOptions.qml:127
msgid "the day as number without a leading zero (1 to 31)"
msgstr "o dia como número sem um zero inicial (1 a 31)"

#: ../AdvancedOptions.qml:128
msgid "the day as number with a leading zero (01 to 31)"
msgstr "o dia como número com um zero a esquerda (01 a 31)"

#: ../AdvancedOptions.qml:129
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "o nome do dia da semana abreviado (ex, \"Seg\" para \"Dom\")."

#: ../AdvancedOptions.qml:130
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr ""
"o nome longo do dia localizado (por exemplo, \"segunda-feira\" a \"domingo"
"\")."

#: ../AdvancedOptions.qml:131
msgid "the month as number without a leading zero (1 to 12)"
msgstr "o mês como número sem um zero inicial (1 a 12)"

#: ../AdvancedOptions.qml:132
msgid "the month as number with a leading zero (01 to 12)"
msgstr "o mês como número com um zero inicial (01 a 12)"

#: ../AdvancedOptions.qml:133
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "o nome abreviado do mês localizado (por exemplo, 'Jan' a 'Dez')."

#: ../AdvancedOptions.qml:134
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr ""
"o nome longo do mês localizado (por exemplo, \"janeiro\" a \"dezembro\")."

#: ../AdvancedOptions.qml:135
msgid "the year as two digit number (00 to 99)"
msgstr "o ano como número de dois dígitos (00 a 99)"

#: ../AdvancedOptions.qml:136
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr ""
"o ano como número de quatro dígitos. Se o ano for negativo, um sinal de "
"menos é adicionado em acréscimo."

#: ../AdvancedOptions.qml:137
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr ""
"a hora sem um zero à esquerda (0 a 23 ou 1 a 12, se a indicação AM / PM)"

#: ../AdvancedOptions.qml:138
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr "a hora com um zero à esquerda (00 a 23 ou 01 a 12, se AM / PM)"

#: ../AdvancedOptions.qml:139
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr "a hora sem um zero inicial (0 a 23, mesmo com a tela AM / PM)"

#: ../AdvancedOptions.qml:140
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr "a hora com um zero inicial (00 a 23, mesmo com exibição AM / PM)"

#: ../AdvancedOptions.qml:141
msgid "the minute without a leading zero (0 to 59)"
msgstr "o minuto sem um zero inicial (0 a 59)"

#: ../AdvancedOptions.qml:142
msgid "the minute with a leading zero (00 to 59)"
msgstr "o minuto com um zero inicial (00 a 59)"

#: ../AdvancedOptions.qml:143
msgid "the second without a leading zero (0 to 59)"
msgstr "o segundo sem um zero inicial (0 a 59)"

#: ../AdvancedOptions.qml:144
msgid "the second with a leading zero (00 to 59)"
msgstr "o segundo com um zero inicial (00 a 59)"

#: ../AdvancedOptions.qml:145
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "e milissegundos sem zeros iniciais (0 a 999)"

#: ../AdvancedOptions.qml:146
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "os milissegundos com zeros à esquerda (000 a 999)"

#: ../AdvancedOptions.qml:147
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "use a exibição AM / PM. AP será substituído por 'AM' ou 'PM'."

#: ../AdvancedOptions.qml:148
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr "use a exibição am / pm. ap será substituído por 'am' ou 'pm'."

#: ../AdvancedOptions.qml:149
msgid "the timezone (for example 'CEST')"
msgstr "o fuso horário (por exemplo, 'CEST')"

#: ../AdvancedOptions.qml:158
msgid "Add to Format"
msgstr "Adicionar ao formato"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: ../AdvancedOptions.qml:189
msgid "Color"
msgstr "Cor"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: ../AdvancedOptions.qml:257
msgid "Alignment"
msgstr "Alinhamento"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:303
msgid "Opacity"
msgstr "Opacidade"

#: ../DeleteDialog.qml:24
msgid "Delete media?"
msgstr "Quer mesmo apagar?"

#: ../DeleteDialog.qml:34 ../PhotogridView.qml:56 ../SlideshowView.qml:75
msgid "Delete"
msgstr "Eliminar"

#: ../DeleteDialog.qml:40 ../ViewFinderOverlay.qml:1122
#: ../ViewFinderOverlay.qml:1136 ../ViewFinderOverlay.qml:1175
#: ../ViewFinderView.qml:494
msgid "Cancel"
msgstr "Cancelar"

#: ../GalleryView.qml:200
msgid "No media available."
msgstr "Sem multimédia disponível."

#: ../GalleryView.qml:235
msgid "Scanning for content..."
msgstr "A procurar por conteúdo..."

#: ../GalleryViewHeader.qml:82 ../SlideshowView.qml:45
msgid "Select"
msgstr "Selecionar"

#: ../GalleryViewHeader.qml:83
msgid "Edit Photo"
msgstr "Editar foto"

#: ../GalleryViewHeader.qml:83
msgid "Photo Roll"
msgstr "Rolo da câmara"

#: ../Information.qml:20
msgid "About"
msgstr "Acerca"

#: ../Information.qml:82
msgid "Get the source"
msgstr "Obter o código fonte"

#: ../Information.qml:83
msgid "Report issues"
msgstr "Reportar problemas"

#: ../Information.qml:84
msgid "Help translate"
msgstr "Ajudar a traduzir"

#: ../MediaInfoPopover.qml:25
msgid "Media Information"
msgstr "Informação da média"

#: ../MediaInfoPopover.qml:30
#, qt-format
msgid "Name : %1"
msgstr "Nome : %1"

#: ../MediaInfoPopover.qml:33
#, qt-format
msgid "Type : %1"
msgstr "Tipo : %1"

#: ../MediaInfoPopover.qml:37
#, qt-format
msgid "Width : %1"
msgstr "Largura : %1"

#: ../MediaInfoPopover.qml:41
#, qt-format
msgid "Height : %1"
msgstr "Altura : %1"

#: ../NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "Sem espaço no dispositivo, liberte algum para continuar."

#: ../PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "Deslize para a esquerda para o rolo de fotografias"

#: ../PhotogridView.qml:42 ../SlideshowView.qml:60
msgid "Share"
msgstr "Partilhar"

#: ../PhotogridView.qml:69 ../SlideshowView.qml:53
msgid "Gallery"
msgstr "Galeria"

#: ../SlideshowView.qml:68
msgid "Image Info"
msgstr "Info da imagem"

#: ../SlideshowView.qml:86
msgid "Edit"
msgstr "Editar"

#: ../SlideshowView.qml:441
msgid "Back to Photo roll"
msgstr "Voltar ao rolo da câmara"

#: ../UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Incapaz de partilhar"

#: ../UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Incapaz de partilhar fotos e vídeos ao mesmo tempo"

#: ../UnableShareDialog.qml:30
msgid "Ok"
msgstr "Ok"

#: ../ViewFinderOverlay.qml:367 ../ViewFinderOverlay.qml:390
#: ../ViewFinderOverlay.qml:418 ../ViewFinderOverlay.qml:441
#: ../ViewFinderOverlay.qml:526 ../ViewFinderOverlay.qml:589
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:42
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:65
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:86
msgid "On"
msgstr "Ligado"

#: ../ViewFinderOverlay.qml:372 ../ViewFinderOverlay.qml:400
#: ../ViewFinderOverlay.qml:423 ../ViewFinderOverlay.qml:446
#: ../ViewFinderOverlay.qml:465 ../ViewFinderOverlay.qml:531
#: ../ViewFinderOverlay.qml:599
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:47
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:70
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:91
msgid "Off"
msgstr "Desligado"

#: ../ViewFinderOverlay.qml:395
msgid "Auto"
msgstr "Automático"

#: ../ViewFinderOverlay.qml:432
msgid "HDR"
msgstr "HDR"

#: ../ViewFinderOverlay.qml:470
msgid "5 seconds"
msgstr "5 segundos"

#: ../ViewFinderOverlay.qml:475
msgid "15 seconds"
msgstr "15 segundos"

#: ../ViewFinderOverlay.qml:493
msgid "Fine Quality"
msgstr "Qualidade alta"

#: ../ViewFinderOverlay.qml:498
msgid "High Quality"
msgstr "Qualidade alta"

#: ../ViewFinderOverlay.qml:503
msgid "Normal Quality"
msgstr "Qualidade média"

#: ../ViewFinderOverlay.qml:508
msgid "Basic Quality"
msgstr "Qualidade baixa"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ../ViewFinderOverlay.qml:541
msgid "SD"
msgstr "SD"

#: ../ViewFinderOverlay.qml:549
msgid "Save to SD Card"
msgstr "Guardar no cartão SD"

#: ../ViewFinderOverlay.qml:554
msgid "Save internally"
msgstr "Guardar no dispositivo"

#: ../ViewFinderOverlay.qml:594
msgid "Vibrate"
msgstr "Vibrar"

#: ../ViewFinderOverlay.qml:1119
msgid "Low storage space"
msgstr "Pouco espaço para guardar"

#: ../ViewFinderOverlay.qml:1120
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr ""
"Está a ficar sem espaço. Para continuar sem interrupções, liberte algum "
"espaço agora."

#: ../ViewFinderOverlay.qml:1133
msgid "External storage not writeable"
msgstr "Armazenamento externo não gravável"

#: ../ViewFinderOverlay.qml:1134
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"Não parece possível escrever no seu armazenamento externo. Tente ejetar e "
"inseri-lo novamente pode resolver o problema, ou talvez você precise formatá-"
"lo."

#: ../ViewFinderOverlay.qml:1172
msgid "Cannot access camera"
msgstr "Impossível aceder à câmara"

#: ../ViewFinderOverlay.qml:1173
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"A app da câmara não tem permissão para aceder ao hardware da câmara ou "
"ocorreu outro erro.\n"
"\n"
"Se conceder permissão não resolver o este problema, reinicie o telemóvel."

#: ../ViewFinderOverlay.qml:1182
msgid "Edit Permissions"
msgstr "Editar permissões"

#: ../ViewFinderView.qml:485
msgid "Capture failed"
msgstr "Captura falhou"

#: ../ViewFinderView.qml:490
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"Substituir o seu armazenamento externo, formatá-lo ou reiniciar o "
"dispositivo pode solucionar o problema."

#: ../ViewFinderView.qml:491
msgid "Restarting your device might fix the problem."
msgstr "Reiniciar o seu dispositivo pode resolver o problema."

#: ../camera-app.qml:57
msgid "Flash"
msgstr "Flash"

#: ../camera-app.qml:58
msgid "Light;Dark"
msgstr "Claro;Escuro"

#: ../camera-app.qml:61
msgid "Flip Camera"
msgstr "Inverter câmara"

#: ../camera-app.qml:62
msgid "Front Facing;Back Facing"
msgstr "Capa frontal;Capa traseira"

#: ../camera-app.qml:65
msgid "Shutter"
msgstr "Obturador"

#: ../camera-app.qml:66
msgid "Take a Photo;Snap;Record"
msgstr "Tirar uma fotografia;Capturar;Gravar"

#: ../camera-app.qml:69
msgid "Mode"
msgstr "Modo"

#: ../camera-app.qml:70
msgid "Stills;Video"
msgstr "Stills;Vídeo"

#: ../camera-app.qml:74
msgid "White Balance"
msgstr "Equilíbrio de brancos"

#: ../camera-app.qml:75
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "Condição de luminosidade;Dia;Nublado;Interior"

#: ../camera-app.qml:380
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "<b>%1</b> fotografias tiradas hoje"

#: ../camera-app.qml:381
msgid "No photos taken today"
msgstr "Sem fotografias tiradas hoje"

#: ../camera-app.qml:391
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "<b>%1</b> vídeos gravados hoje"

#: ../camera-app.qml:392
msgid "No videos recorded today"
msgstr "Sem vídeos gravados hoje"

#: camera-app.desktop.in.in.h:1
msgid "Camera"
msgstr "Câmara"

#: camera-app.desktop.in.in.h:2
msgid "Camera application"
msgstr "Câmara"

#: camera-app.desktop.in.in.h:3
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "Fotos;Vídeos;Captura;Disparo;Snapshot;Gravar"

#~ msgid "Version %1"
#~ msgstr "Versão: %1"

#~ msgid "Advanced Options"
#~ msgstr "Opções avançadas"
